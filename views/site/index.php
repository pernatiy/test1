<?php

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ArrayDataProvider */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function($model){
            return $model['title'] . " (Автор:" . $model['autor']['name'] . ")";
        },
    ]) ?>


</div>

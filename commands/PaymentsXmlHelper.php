<?php

namespace app\commands;

use yii\console\Exception;
use yii\helpers\ArrayHelper;

class PaymentsXmlHelper
{

    public static function getBrokenPayments()
    {
        $total = \Yii::$app->db->createCommand(<<<SQL
            SELECT COUNT(p.id) FROM payments p
            LEFT JOIN credits c ON c.id = p.cred_id
            WHERE c.id IS NULL
SQL
        )->queryScalar();
        $batchSize = 1000;
        $currentPaymentId = 0;
        for ($i = 0; $i < ceil($total / $batchSize); $i++) {
            $payments = \Yii::$app->db->createCommand(<<<SQL
                SELECT p.id, p.cred_id, p.data_set FROM `payments` `p`
                LEFT JOIN credits c ON c.id = p.cred_id
                WHERE c.id IS NULL AND p.id > :currentPaymentId
                LIMIT :limit
SQL
                , [
                    ':currentPaymentId' => $currentPaymentId,
                    ':limit' => $batchSize,
                ])->queryAll();
            // "1) вытащит все записи из таблицы payments у которых нет соответствующей записи в таблице credits"
            // TODO: Получил что дальше делать - непонятно
            echo ".";
        }
    }

    /***
     * Create Xml Payment file
     *
     * @throws \yii\db\Exception
     */

    public static function createPaymentsXml()
    {

        $xmlFile = \Yii::getAlias('@app/runtime/file.xml');
        if (file_exists($xmlFile)) {
            unlink($xmlFile);
        }
        file_put_contents($xmlFile, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '<payments>');

        $total = \Yii::$app->db->createCommand(<<<SQL
            SELECT COUNT(p.id) FROM payments p
            LEFT JOIN credits c ON c.id = p.cred_id
            WHERE NOT c.id IS NULL
SQL
        )->queryScalar();


        $batchSize = 1000;
        $currentPaymentId = 0;
        for ($i = 0; $i < ceil($total / $batchSize); $i++) {
            $payments = \Yii::$app->db->createCommand(<<<SQL
                SELECT p.id, p.cred_id, p.data_set FROM `payments` `p`
                LEFT JOIN credits c ON c.id = p.cred_id
                WHERE NOT c.id IS NULL AND p.id > :currentPaymentId
                LIMIT :limit
SQL
                , [
                    ':currentPaymentId' => $currentPaymentId,
                    ':limit' => $batchSize,
                ])->queryAll();

            $xmlData = '';
            foreach ($payments as $payment) {
                $currentPaymentId = $payment['id'];
                $xmlData .= \Yii::$app->controller->renderFile(\Yii::getAlias('@app/commands/xml_payment.php'), [
                    'payment' => $payment,
                ]);
            }

            file_put_contents($xmlFile, $xmlData, FILE_APPEND);

        }

        file_put_contents($xmlFile, PHP_EOL . '</payments>', FILE_APPEND);
    }

    /**
     * Validate XML File by XSD
     *
     * @return bool
     * @throws Exception
     */

    public static function validateXml()
    {
        $xmlFile = \Yii::getAlias('@app/runtime/1.xml');
        if (!file_exists($xmlFile)) {
            throw new Exception('file not found');
        }

        $xsd = \Yii::getAlias('@app/commands/schema.xsd');

        libxml_use_internal_errors(true);

        $xml = new \DOMDocument();
        $xml->load($xmlFile);

        if (!$xml->schemaValidate($xsd)) {
            $errorLogFile = \Yii::getAlias('@app/runtime/xml_error.log');
            $errorList = self::getXmlErrors();
            file_put_contents($errorLogFile, $errorList);
            return false;
        }

    }

    /**
     * Xml Error Handler
     *
     * @param $error
     * @return string
     */

    public static function getXmlError($error)
    {
        $return = PHP_EOL;
        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }
        $return .= trim($error->message);
        if ($error->file) {
            $return .= " in $error->file";
        }
        $return .= " on line $error->line" . PHP_EOL;

        return $return;
    }

    /***
     *  get XML errors list data
     *
     */
    public static function getXmlErrors()
    {
        $errors = libxml_get_errors();
        $errorsStr = '';
        foreach ($errors as $error) {
            $errorsStr .= self::getXmlError($error);
        }
        libxml_clear_errors();

        return $errorsStr;
    }

}
<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Payments;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class XmlController extends Controller
{
    /*
     * Set app memory limit
     * */
    public function init()
    {
        ini_set("memory_limit", "8M");

        parent::init();
    }


    /**
     * Task1: getBrokenPayments
     * @return int Exit code
     */
    public function actionTask1()
    {
        PaymentsXmlHelper::getBrokenPayments();

        return ExitCode::OK;
    }

    /**
     * Task2: Create XML Payments feed
     * @return int Exit code
     */
    public function actionTask2()
    {

        PaymentsXmlHelper::createPaymentsXml();

        return ExitCode::OK;
    }

    /**
     * Task3: validate XML
     * @return int Exit code
     */
    public function actionTask3()
    {

        PaymentsXmlHelper::validateXml();

        return ExitCode::OK;
    }


}

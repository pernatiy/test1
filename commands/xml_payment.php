<?php
use yii\helpers\ArrayHelper;

/** @var $payment array */

$data = \Opis\Closure\unserialize($payment['data_set']);
$overdue = ArrayHelper::getValue($data, 'overdue');
?>
<payment id="<?=$payment['id']?>">
    <cred_id><?=$payment['cred_id']?></cred_id>
    <overdue><?=$overdue?></overdue>
</payment>

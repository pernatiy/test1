<?php

namespace app\modules\admin;

use yii\helpers\Url;
use Yii;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if( Yii::$app->user->isGuest ){
            $loginUrl = Url::to(['/site/login']);
            Yii::$app->response->redirect($loginUrl);
            Yii::$app->response->send();
        }

        parent::init();

        // custom initialization code goes here
    }
}

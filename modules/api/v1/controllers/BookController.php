<?php


namespace app\modules\api\v1\controllers;


use app\models\Book;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

class BookController extends ActiveController
{
    public $modelClass = Book::class;

}
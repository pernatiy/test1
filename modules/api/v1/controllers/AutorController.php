<?php


namespace app\modules\api\v1\controllers;


use app\models\Autor;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

class AutorController extends ActiveController
{
    public $modelClass = Autor::class;

}